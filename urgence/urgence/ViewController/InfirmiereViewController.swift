//
//  InfirmiereViewController.swift
//  urgence
//
//  Created by dae2 on 2019-11-01.
//  Copyright © 2019 dae2. All rights reserved.
//

import UIKit

class InfirmiereViewController: UIViewController
{
    public var NurseTab: [Nurse] = []
    var state = 0;
    @IBOutlet weak var First_NameTextField: UITextField!
    @IBOutlet weak var Last_NameTextField: UITextField!
    @IBOutlet weak var Button_Right: UIBarButtonItem!
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var Employee_NumberLabel: UILabel!
    @IBOutlet weak var Actual_PasswordTextField: UITextField!
    @IBOutlet weak var New_PasswordTextField: UITextField!
    @IBOutlet weak var Confirm_New_PasswordTextField: UITextField!
    var valide = false;

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NurseTab = (self.tabBarController as! InfTabViewController).NurseTab
        
        First_NameTextField.text = NurseTab[0].First_Name;
        Last_NameTextField.text = NurseTab[0].Last_Name;
        Employee_NumberLabel.text = String(NurseTab[0].Nurse_Id);
    }
    
    @IBAction func logOut(_ sender: Any)
    {
        self.performSegue(withIdentifier: "Show_Main", sender: nil);
    }    
    
    @IBAction func Supression(_ sender: Any) {
        
        let Alert = UIAlertController(title: "Attention", message: "Vous êtes sur le point de supprimer le compte", preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "Confirmer", style: .destructive, handler: { (_) in
            MarthaRequest.deleteNurse(id: self.NurseTab[0].Nurse_Id){_ in
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "Show_Main", sender: nil)
                }
            }
            
        }))
        
        Alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
        
        self.present(Alert, animated: true)
        }
    
    
    @IBAction func modification(_ sender: Any)
    {
        switch state {
        case 0:
            First_NameTextField.isEnabled = true;
            Last_NameTextField.isEnabled = true;
            Button_Right.title = "Valider";
            PasswordView.isHidden = false;
            state = 1;
            break;
            
        case 1:
            
            var Message_Erreur = "";
            var Password_Temp = "";
            
            if(NurseTab[0].Password != Actual_PasswordTextField.text)
            {
                Message_Erreur = "Mot de passe actuel en erreur";
            }
            else if(New_PasswordTextField.text != Confirm_New_PasswordTextField.text)
            {
                Message_Erreur = "Mot de passe non identique";
            }
            else
            {
                valide = true;
            }
            
            
            if(valide)
            {
                if ((self.Confirm_New_PasswordTextField.text)!.isEmpty) {
                    
                    //tempPasswd = self.Confirm_New_PasswordTextField.text!
                    Password_Temp = self.Actual_PasswordTextField.text!
                }
                else {
                   // tempPasswd = self.Actual_PasswordTextField.text!
                    Password_Temp = self.Confirm_New_PasswordTextField.text!
                }
                
                MarthaRequest.updateNurse(id: self.NurseTab[0].Nurse_Id,last_name: self.Last_NameTextField.text!, first_name: First_NameTextField.text!, passwd: Password_Temp){ _ in
                    
                    DispatchQueue.main.async {
                       
                        
                    }
                }
                
                First_NameTextField.isEnabled = false;
                Last_NameTextField.isEnabled = false;
                Button_Right.title = "Modifier";
                PasswordView.isHidden = true;
                state = 0;
            }
            else
            {
                let Alert = UIAlertController(title: "Erreur", message: Message_Erreur, preferredStyle: .alert)
                
                Alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    self.New_PasswordTextField.text = ""
                    self.Confirm_New_PasswordTextField.text = ""
                }))
                self.present(Alert, animated: true)
            }
            break;
        default:
            break;
        }
    }
}
