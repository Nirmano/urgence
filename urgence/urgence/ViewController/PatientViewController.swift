//
//  PatientController.swift
//  urgence
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 dae2. All rights reserved.
//
// mettre validation sur les lettre et nombre dans le priority
//


import UIKit

class PatientViewController: UIViewController
{
    private var NurseTab: [Nurse] = []
    private var PatientTab: [Patient] = []
    private var caseTab: [Cases] = []
    @IBOutlet weak var NAMTextField: UITextField!
    @IBOutlet weak var First_NameTextField: UITextField!
    @IBOutlet weak var Last_NameTextField: UITextField!
    var valide = false;
    @IBOutlet weak var GenderPicker: UISegmentedControl!
    @IBOutlet weak var PrioritySegmentedControler: UISegmentedControl!
    @IBOutlet weak var BirthdayDatePicker: UIDatePicker!
    @IBOutlet weak var RightButton: UIBarButtonItem!
    @IBOutlet weak var DeleteButton: UIButton!
    
    var AddOrModifie = ""
    var NAMPatient = ""
    var Type_Employee = ""
    var PriorityPatient = 0
    var Chambre = true
    
    let dateFormatter = DateFormatter()
    

    
    override func viewDidLoad() {
        
        Type_Employee =  self.tabBarController is DocTabViewController ? "2" : "1"
        
        if(Type_Employee == "1")
        {
            NurseTab = (self.tabBarController as! InfTabViewController).NurseTab
        }
   //     print("nusrse: ",(self.tabBarController as! InfTabViewController).NurseTab)
        
        if(AddOrModifie == "1")
        {
            MarthaRequest.fetchPatient(id : NAMPatient, completion:
                {
                    (patients) in
                    if let fetchPatients = patients
                    {
                        self.PatientTab = fetchPatients
                        DispatchQueue.main.async
                            {
                                self.NAMTextField.text = self.PatientTab[0].NAM
                                self.First_NameTextField.text = self.PatientTab[0].First_Name
                                self.Last_NameTextField.text = self.PatientTab[0].Last_Name
                                self.GenderPicker.selectedSegmentIndex = self.PatientTab[0].Gender == "homme" ? 0 : 1
                                self.dateFormatter.dateFormat = "yyyy-MM-dd"
                                self.BirthdayDatePicker.date = self.dateFormatter.date(from: self.PatientTab[0].Birthday)!
                        }
                    }
            })
            
            
            NAMTextField.isEnabled = false;
            First_NameTextField.isEnabled = false;
            Last_NameTextField.isEnabled = false;
            GenderPicker.isEnabled = false;
            BirthdayDatePicker.isEnabled = false;
            RightButton.title = "Modifier"
            DeleteButton.isHidden = false
            PrioritySegmentedControler.selectedSegmentIndex = PriorityPatient - 1
            
            if(Chambre)
            {
                PrioritySegmentedControler.isEnabled = false;
                RightButton.title = "";
                RightButton.isEnabled = false;
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        if(AddOrModifie == "1")
        {
            MarthaRequest.fetchPatient(id : NAMPatient, completion:
                {
                    (patients) in
                    if let fetchPatients = patients
                    {
                        self.PatientTab = fetchPatients
                        DispatchQueue.main.async
                            {
                                self.NAMTextField.text = self.PatientTab[0].NAM
                                self.First_NameTextField.text = self.PatientTab[0].First_Name
                                self.Last_NameTextField.text = self.PatientTab[0].Last_Name
                                self.GenderPicker.selectedSegmentIndex = self.PatientTab[0].Gender == "homme" ? 0 : 1
                                self.dateFormatter.dateFormat = "yyyy-MM-dd"
                                self.BirthdayDatePicker.date = self.dateFormatter.date(from: self.PatientTab[0].Birthday)!
                        }
                    }
            })
            
            NAMTextField.isEnabled = false;
            First_NameTextField.isEnabled = false;
            Last_NameTextField.isEnabled = false;
            GenderPicker.isEnabled = false;
            BirthdayDatePicker.isEnabled = false;
            RightButton.title = "Modifier"
            DeleteButton.isHidden = false
            PrioritySegmentedControler.selectedSegmentIndex = PriorityPatient - 1

                
            if(Chambre)
            {
                PrioritySegmentedControler.isEnabled = false;
                RightButton.title = "";
                RightButton.isEnabled = false;
            }
        }
    }
    
    @IBAction func rightButtonAddPatient(_ sender: Any)
    {
        if(AddOrModifie == "1")
        {
            MarthaRequest.updateCasesPat(NAM: NAMTextField.text!, priority: PrioritySegmentedControler.selectedSegmentIndex + 1) { _ in
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "Return_List", sender: self)
                }
            }
        }
        else
        {
            var Message_Erreur = "";
            if(Last_NameTextField.text == "")
            {
                Message_Erreur = "Nom vide";
            }
            else if(First_NameTextField.text == "")
            {
                Message_Erreur = "Prenom vide";
            }
            else if(NAMTextField.text == "")
            {
                Message_Erreur = "NAM vide";
            }
            else
            {
                valide = true;
            }
            
            dateFormatter.locale = Locale(identifier: "ja_JP")
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            let DateTemporaire = dateFormatter.string(from: BirthdayDatePicker.date)
            
            print(DateTemporaire)
            
            if(valide)
            {
                let Gender = GenderPicker.selectedSegmentIndex == 0 ? "homme" : "femme"
                
                MarthaRequest.insertPatient(nam: NAMTextField.text!, last_name:Last_NameTextField.text!, first_name: First_NameTextField.text!, gender: Gender, birthday: DateTemporaire)  { _ in
                    
                    DispatchQueue.main.async
                        {
                            self.callInsertCases()
                    }
                }
            }
            else
            {
                let Alert = UIAlertController(title: "Erreur", message: Message_Erreur, preferredStyle: .alert)
                
                Alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
                    {
                        (_) in
                }))
                self.present(Alert, animated: true)
            }
        }
        
    }
    
    func callInsertCases()
    {
        MarthaRequest.insertCases(nam: NAMTextField.text!, nurse_id:NurseTab[0].Nurse_Id , priority: PrioritySegmentedControler.selectedSegmentIndex + 1){
            _ in
            
            DispatchQueue.main.async
            {
                 self.performSegue(withIdentifier: "Return_List", sender: self)
            }
        }
    }
    
    @IBAction func deletePatient(_ sender: Any)
    {
        let Alert = UIAlertController(title: "Attention", message: "Vous êtes sur le point de supprimer un patient", preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "Confirmer", style: .destructive, handler: { (_) in

            MarthaRequest.updateRoomPatNul(NAM: self.NAMPatient , completion: { _ in
                
                DispatchQueue.main.async {
                
                                        MarthaRequest.deleteCase(nam: self.NAMTextField.text!, completion: { _ in
                                            DispatchQueue.main.async{
                                                
                                                MarthaRequest.deletePatient(nam: self.NAMTextField.text!, completion: { _ in
                                                    DispatchQueue.main.async {
                                                        //laisse la liste chambre vers le patient et non la liste (Liste de chambre != liste chambre)
                                                        // revenir a la derniere view , plus un undwind ???
                                                        if(!self.Chambre)
                                                        {
                                                            self.performSegue(withIdentifier: "Return_List", sender: self)
                                                        }
                                                        else
                                                        {
                                                            self.performSegue(withIdentifier: "Return_Chambre", sender: self)
                                                        }
                                                       
                                                    }
                                                })
                                            }
                                        })
                    
                }
            
        })
            
        }))
            
            Alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
            
            self.present(Alert, animated: true)
    }

    
}
