//
//  ListePatientController.swift
//  urgence
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 dae2. All rights reserved.
//

import UIKit

class ListePatientController: UITableViewController
{
    private var caseTab: [Cases] = []
    private var Case_Priority = 0
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Show_ModPatient"
        {
            if let Controler = segue.destination as? PatientViewController
            {
                Controler.AddOrModifie = "1"
                Controler.PriorityPatient = Case_Priority
                Controler.Chambre = false
                Controler.NAMPatient = caseTab[(tableView.indexPathForSelectedRow?.row)!].NAM
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Case_Priority = caseTab[indexPath.row].Priority
        
        self.performSegue(withIdentifier: "Show_ModPatient", sender: nil);
    }
    
    @IBAction func logOut(_ sender: Any)
    {
        self.performSegue(withIdentifier: "Show_Main", sender: nil);
    }
    
    @IBAction func addPatient(segue:UIStoryboardSegue)
    {
    }
    
    override func viewDidLoad() {
        
        MarthaRequest.fetchCases(id: 0, completion: { (cases) in if let fetchCases = cases {
            self.caseTab = fetchCases
            
            print("casetab",self.caseTab)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
            
        }
    })
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        MarthaRequest.fetchCases(id: 0, completion: { (cases) in if let fetchCases = cases {
            self.caseTab = fetchCases
            
            print("casetab",self.caseTab)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
            
            }
        })
        
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return caseTab.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Patient" , for:indexPath)
        cell.textLabel?.text = "NAM: " + caseTab[indexPath.row].NAM + " Priorité: " + (String)(caseTab[indexPath.row].Priority)
        
        return cell
    }
    
    
}
