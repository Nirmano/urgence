//
//  DocteurViewController.swift
//  urgence
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 dae2. All rights reserved.
//
//faire une delete sur l'ensemble des tables ayant le docteur soit DOCTOR, ROOM, CASE
//

import UIKit

class DocteurViewController: UIViewController
{
    private var DocTab: [Doctor] = []
    var state = 0
    @IBOutlet weak var Button_Right: UIBarButtonItem!
    @IBOutlet weak var First_NameTextField: UITextField!
    @IBOutlet weak var Last_NameTextField: UITextField!
    @IBOutlet weak var SpecialisationTextField: UITextField!
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var Employee_NumberLabel: UILabel!
    @IBOutlet weak var Actual_PasswordTextField: UITextField!
    @IBOutlet weak var New_PasswordTextField: UITextField!
    @IBOutlet weak var Confirm_New_PasswordTextField: UITextField!
    var valide = false;

    @IBAction func logOut(_ sender: Any)
    {
        self.performSegue(withIdentifier: "Show_Main", sender: nil);
    } 
    
    @IBAction func supression(_ sender: Any)
    {
        let Alert = UIAlertController(title: "Attention", message: "Vous êtes sur le point de supprimer le compte", preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "Confirmer", style: .destructive, handler:
        {
            (_) in
            MarthaRequest.updateRoomNoDoc( Doctor_id: self.DocTab[0].Doctor_id){
                _ in
                DispatchQueue.main.async{
                    MarthaRequest.deleteDoc(id: self.DocTab[0].Doctor_id){_ in
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "Show_Main", sender: nil)
                        }
                    }
                }
            }
        }
    ))
        
        Alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
        
        self.present(Alert, animated: true)
    }
    
    
    @IBAction func modification(_ sender: Any)
    {
        switch state
        {
            case 0:
                First_NameTextField.isEnabled = true
                Last_NameTextField.isEnabled = true
                SpecialisationTextField.isEnabled = true; Button_Right.title = "Valider"
                PasswordView.isHidden = false
                state = 1
                break;
            
            case 1:
                
                var Message_Erreur = "";
                var Password_Temp = ""
                if(DocTab[0].Password != Actual_PasswordTextField.text)
                {
                    Message_Erreur = "Mot de passe actuel en erreur";
                }
                else if(New_PasswordTextField.text != Confirm_New_PasswordTextField.text)
                {
                    Message_Erreur = "Mot de passe non identique";
                }
                else
                {
                    valide = true;
                }
                
                
                if(valide)
                {
                    if ((self.Confirm_New_PasswordTextField.text)!.isEmpty) {
                        
                        //tempPasswd = self.Confirm_New_PasswordTextField.text!
                        Password_Temp = self.Actual_PasswordTextField.text!
                    }
                    else {
                        // tempPasswd = self.Actual_PasswordTextField.text!
                        Password_Temp = self.Confirm_New_PasswordTextField.text!
                    }
                    
                    MarthaRequest.updateDoc(id: self.DocTab[0].Doctor_id,last_name: self.Last_NameTextField.text!, first_name: First_NameTextField.text!, passwd: Password_Temp, speciality: SpecialisationTextField.text ?? ""){ _ in
                        
                        DispatchQueue.main.async {
                            
                            
                        }
                    }
                    First_NameTextField.isEnabled = false;
                    Last_NameTextField.isEnabled = false;
                    Button_Right.title = "Modifier";
                    PasswordView.isHidden = true;
                    state = 0;
                }
                else
                {
                    let alert = UIAlertController(title: "Erreur", message: Message_Erreur, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                        self.New_PasswordTextField.text = ""
                        self.Confirm_New_PasswordTextField.text = ""
                    }))
                    self.present(alert, animated: true)
                }
                break;
            
            default:
                break;
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        DocTab = (self.tabBarController as! DocTabViewController).DocTab
        First_NameTextField.text = DocTab[0].First_Name;
        Last_NameTextField.text = DocTab[0].Last_Name;
        Employee_NumberLabel.text = String(DocTab[0].Doctor_id);
        SpecialisationTextField.text = DocTab[0].Speciality;
    }
    
    
}





