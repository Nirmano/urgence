//
//  MainViewController.swift
//  urgence
//
//  Created by dae2 on 2019-11-01.
//  Copyright © 2019 dae2. All rights reserved.
//

import UIKit

class MainViewController: UIViewController
{
  
    @IBOutlet weak var Employee_Number: UITextField!
    @IBOutlet weak var Password: UITextField!
    var NurseTab: [Nurse] = []
    var DocTab: [Doctor] = []
    
    @IBAction func logOut(segue:UIStoryboardSegue)
    {
        Employee_Number.text = "";
        Password.text = "";
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    func containsOnlyLetters(input: String) -> Bool {
        for chr in input {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let Controler = segue.destination as? InfTabViewController
        {
            Controler.NurseTab = self.NurseTab;
        }
        
        if let controler = segue.destination as? DocTabViewController
        {
            controler.DocTab = self.DocTab;
        }
    }
    var Employe_Number_Int = 0;
    
    @IBAction func connexionEmploye(_ sender: Any)
    {
       
        if(!containsOnlyLetters(input: Employee_Number.text!) && !Employee_Number.text!.isEmpty){
            Employe_Number_Int = Int(Employee_Number.text!)!
            MarthaRequest.fetchDoctorLog( id: Employe_Number_Int,passwd: Password.text!, completion:
                {
                    (doctors) in
                    if let fetchDoctors = doctors
                    {
                        
                        self.DocTab = fetchDoctors
                        print(fetchDoctors)
                        DispatchQueue.main.async
                            {
                                if(self.DocTab.isEmpty == false)
                                {
                                    self.performSegue(withIdentifier: "Show_Doc", sender: nil);
                                }
                                else
                                {
                                    self.NurseMartha()
                                }
                        }
                    }
            })
        }
        else{
            nonValide()
        }
    }
    
    func NurseMartha()
    {
        if(!Employee_Number.text!.isEmpty)
        {
            Employe_Number_Int = Int(Employee_Number.text!)!
        }
        
        MarthaRequest.fetchNurse( id: Employe_Number_Int,passwd: Password.text!, completion:
        {
            (infirmieres) in
            if let fetchInfirmiere = infirmieres
            {
              
                self.NurseTab = fetchInfirmiere

                print(fetchInfirmiere)
                DispatchQueue.main.async
                {
                    if (self.NurseTab.isEmpty == false)
                    {
                        self.performSegue(withIdentifier: "Show_Inf", sender: nil);
                    }
                    else
                    {
                        self.nonValide()
                    }
                }
            }
        })
    }
    
    func nonValide()
    {
        //information de connexion invalide
        var Message_Erreur = "";
        if(self.Employee_Number.text == "")
        {
            Message_Erreur = "Numéro d'employé vide";
        }
        else if(self.Password.text == "")
        {
            Message_Erreur = "Mot de passe vide";
        }
        else if(containsOnlyLetters(input: Employee_Number.text!)){
            Message_Erreur = "Numéro d'employé contient des lettres";
            
        }
        else
        {
            Message_Erreur = "Information de connexion invalide";
        }
        
        let Alert = UIAlertController(title: "Erreur", message: Message_Erreur, preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.Employee_Number.text = "";
            self.Password.text = "";
        }))
        
        self.present(Alert, animated: true)
    }
}
