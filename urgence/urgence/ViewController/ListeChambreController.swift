//
//  ListeChambreController.swift
//  urgence
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 dae2. All rights reserved.
//


//double section : mettre 2 section et ensuite quand fecth séparer dans 2 tableaux si cases null ou pas
//afficher dans chaque section 1 seul tableau
//Faire en sorte que l'on peut ajouter une chambre a un doctor si celle-ci n'a pas de doctor
import UIKit

class ListeChambreController : UITableViewController
{
    private var DocTab: [Doctor] = []
    private var RoomTabFull: [Room] = []
    private var RoomTabEmpty: [Room] = []
    private var RoomTabNoDoc: [Room] = []
    private var RoomTemp: [Room] = []
    private var patient : Cases? = nil
    private var caseTab: [Cases] = []
    var flag = 0
    private var Case_Priority = 0
    
    
    //type =1 : infirmiere , type=2: Docteur
    var Type_Employee = ""
    
    
    @IBOutlet weak var Right_Button: UIButton!
    
    @IBAction func ReturnList(segue:UIStoryboardSegue)
    {
        
    }
    
    @IBAction func addRoom(_ sender: Any)
    {
        let Message_Ajout = "Vous allez ajouter une chambre a votre compte"
        let Alert = UIAlertController(title: "Ajout", message: Message_Ajout, preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "Confirmer", style: .destructive, handler:
            {
                (_) in
                MarthaRequest.insertRoom( doctor_id: self.DocTab[0].Doctor_id)
                {
                    _ in
                    DispatchQueue.main.async {
                        self.viewDidAppear(true)
                    }
                }
        }))
        Alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
        
        self.present(Alert, animated: true)
    }
    
    @IBAction func logOut(_ sender: Any)
    {
        self.performSegue(withIdentifier: "Show_Main", sender: nil);
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        Type_Employee =  self.tabBarController is DocTabViewController ? "2" : "1"
        
        if(Type_Employee == "2")
        {
            Right_Button.setTitle("Ajouter", for: .normal)
            DocTab = (self.tabBarController as! DocTabViewController).DocTab
            
            MarthaRequest.fetchRoom(id:DocTab[0].Doctor_id, completion:
                {
                    (rooms) in
                    if let fetchRooms = rooms
                    {
                        self.RoomTemp = fetchRooms
                        
                        for Room in self.RoomTemp{
                            if(Room.Cases_Id != nil && Room.Doctor_Id != nil){
                                self.RoomTabFull.append(Room)
                            }
                            else if(Room.Cases_Id == nil && Room.Doctor_Id != nil){
                                self.RoomTabEmpty.append(Room)
                            }
                            else{
                                self.RoomTabNoDoc.append(Room)
                            }
                        }
                        
                        //print(fetchRooms)
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
            })
        }
        else
        {
            Right_Button.isEnabled = false;
            Right_Button.isHidden = true;
           // Right_Button.setTitle("Modifier", for: .normal)
            
            MarthaRequest.fetchRoomAll(id:0, completion:
                {
                    (rooms) in
                    if let fetchRooms = rooms
                    {
                        
                        self.RoomTemp = fetchRooms
                        for Room in self.RoomTemp{
                            if(Room.Cases_Id != nil){
                                self.RoomTabFull.append(Room)
                            }
                            else{
                                self.RoomTabEmpty.append(Room)
                            }
                        }
                        print("allo \(self.RoomTabEmpty) ")
                        
                        print(self.RoomTabFull)
                        
                        DispatchQueue.main.async
                            {
                                self.tableView.reloadData()
                        }
                    }
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(flag == 1)
        {
        RoomTabFull.removeAll(keepingCapacity: false)
        RoomTabEmpty.removeAll(keepingCapacity: false)
        RoomTabNoDoc.removeAll(keepingCapacity: false)
        RoomTemp.removeAll(keepingCapacity: false)
        
        if(Type_Employee == "2")
        {
        MarthaRequest.fetchRoom(id:DocTab[0].Doctor_id, completion:
            {
                (rooms) in
                if let fetchRooms = rooms
                {
                    self.RoomTemp = fetchRooms
                    
                    for Room in self.RoomTemp{
                        if(Room.Cases_Id != nil && Room.Doctor_Id != nil){
                            self.RoomTabFull.append(Room)
                        }
                        else if(Room.Cases_Id == nil && Room.Doctor_Id != nil){
                            self.RoomTabEmpty.append(Room)
                        }
                        else{
                            self.RoomTabNoDoc.append(Room)
                        }
                    }
                    
                    //print(fetchRooms)
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
        })
    }
    else
    {
        Right_Button.isEnabled = false;
        Right_Button.isHidden = true;
    // Right_Button.setTitle("Modifier", for: .normal)
    
    MarthaRequest.fetchRoomAll(id:0, completion:
    {
    (rooms) in
    if let fetchRooms = rooms
    {
    
    self.RoomTemp = fetchRooms
    for Room in self.RoomTemp{
    if(Room.Cases_Id != nil){
    self.RoomTabFull.append(Room)
    }
    else{
    self.RoomTabEmpty.append(Room)
    }
    }
    print("allo \(self.RoomTabEmpty) ")
    
    print(self.RoomTabFull)
    
    DispatchQueue.main.async
    {
    self.tableView.reloadData()
    }
    }
    })
    }
        
        self.tableView.reloadData()
        
    }
        
        flag = 1
    }
    
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        if(section < 2){
            label.text = section == 0 ? "Occupées" : "Libres"
        }
        else{
            
            label.text = "Aucun docteur"
        }
        
        label.backgroundColor = UIColor.lightGray
        return label
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if(Type_Employee == "1"){
            return 2
        }
        else{
            return 3
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0)
        {
            return RoomTabFull.count
        }
        else if (section == 1){
            return RoomTabEmpty.count
        }
        
        return RoomTabNoDoc.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var chamber = " "
        var subs = " "
        //let cell = tableView.dequeueReusableCell(withIdentifier: "Chambre" , for:indexPath)
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value2, reuseIdentifier: "cellId")
        
        
        //cell.textLabel?.text = "Chambre # " + String(RoomTabFull[indexPath.row].Room_Id)
        if(indexPath.section<2){
            chamber = indexPath.section == 0 ? String(RoomTabFull[indexPath.row].Room_Id) : String(RoomTabEmpty[indexPath.row].Room_Id)
            
            if(indexPath.section == 0){
                
                subs = String(RoomTabFull[indexPath.row].Cases_Id!)
            }
            
        }
        else{
            chamber = String(RoomTabNoDoc[indexPath.row].Room_Id)
            subs = RoomTabNoDoc[indexPath.row].Cases_Id != nil ? " \( RoomTabNoDoc[indexPath.row].Cases_Id!)" : " "
        }
        
        
        
        //cell.detailTextLabel?.text = "patient #"
        if(subs != " ")
        {
            cell.detailTextLabel?.text = "patient #" + subs
        }
        cell.textLabel?.text = "Chambre # " + chamber
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 2)
        {
            RoomTemp = RoomTabNoDoc
        }
        else if(indexPath.section == 1)
        {
            RoomTemp = RoomTabEmpty
        }
        else
        {
            RoomTemp = RoomTabFull
        }
        
            let Alert = UIAlertController(title: "Chambre", message: "Vous avez selectionner la chambre : \(RoomTemp[indexPath[1]].Room_Id) ", preferredStyle: .alert)
        
        if(indexPath.section == 2)
        {
            Alert.addAction(UIAlertAction(title: "Ajouter", style: .default,handler: { (_) in
                self.addRoomDoc(room_id: self.RoomTemp[indexPath[1]].Room_Id, doctor_id: self.DocTab[0].Doctor_id)
            }))
        }
        if(indexPath.section > 0)
        {
            if (Type_Employee == "2")
            {
                Alert.addAction(UIAlertAction(title: "Supprimer", style: .destructive, handler: { (_) in
                self.deleteChamber(Room_id: self.RoomTemp[indexPath[1]].Room_Id, Case_Id: self.RoomTemp[indexPath[1]].Cases_Id ?? 0)
                }))
            }
            else
            {
                Alert.addAction(UIAlertAction(title: "Assigner", style: .default, handler: { (_) in
                    self.assignPatient(Room_Id: self.RoomTemp[indexPath[1]].Room_Id)
                }))
            }
        }
        
        if(indexPath.section == 0)
        {
            Alert.addAction(UIAlertAction(title: "Voir Patient", style: .default, handler: { (_) in
                MarthaRequest.fetchPatientwithCases(id : self.RoomTemp[indexPath[1]].Cases_Id!, completion:
                    {
                        (patients) in
                        if let fetchPatients = patients
                        {
                            self.patient = fetchPatients[0]
                            print(self.patient!)
                            DispatchQueue.main.async
                                 {
                                    self.Case_Priority = self.patient!.Priority
                                    self.performSegue(withIdentifier: "Show_Patient", sender: nil)
                            }
                        }
                })
                
                
            }))
            
        }
        
        Alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
        
        self.present(Alert, animated: true)
 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {

        if let destination = segue.destination as? PatientViewController
        {
            destination.AddOrModifie = "1"
            destination.NAMPatient = self.patient!.NAM
            destination.PriorityPatient = Case_Priority
        }
    }

    func assignPatient(Room_Id :Int)
    {
        MarthaRequest.fetchCases(id: 0, completion: { (cases) in if let fetchCases = cases {
            self.caseTab = fetchCases
            
            print("casetab",self.caseTab)
            
            DispatchQueue.main.async
            {
                MarthaRequest.updateRoom(room_id: Room_Id , case_id: self.caseTab[0].Cases_Id!, completion: { _ in
                    DispatchQueue.main.async{
                        self.viewDidAppear(true)
                    }
                })
            }
            
            }
        })
        
    }
    
    func addRoomDoc(room_id :Int , doctor_id :Int) {
        let Alert = UIAlertController(title: "Attention", message: "Vous êtes sur le point d'ajouter la chambre a vos chambre disponible", preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "Confirmer", style: .default, handler: { (_) in
                MarthaRequest.updateRoomPutDoc(room_id: room_id, doctor_id: doctor_id) { _ in
                    DispatchQueue.main.async {
                        self.viewDidAppear(true)
                    }
                }
        }))
        
        Alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
        
        self.present(Alert, animated: true)
    }
    
    func deleteChamber(Room_id : Int, Case_Id : Int)
    {
        let Alert = UIAlertController(title: "Attention", message: "Vous êtes sur le point de supprimer la chambre", preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "Confirmer", style: .destructive, handler: { (_) in
            
            if(Case_Id == 0)
            {
                MarthaRequest.deleteRoom(id: Room_id ){ _ in
                DispatchQueue.main.async {
                    self.viewDidAppear(true)
                }}
            }
            else
            {
                let Alert2 = UIAlertController(title: "Attention", message: "La Chambre a un patient, la requete a été abondoné", preferredStyle: .alert)
                Alert2.addAction(UIAlertAction(title: "D'accord", style: .cancel))
                
                self.present(Alert2, animated: true)
            }
            
            
        }))
        
        Alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
        
        self.present(Alert, animated: true)
    }
    
}
