//
//  ViewController.swift
//  urgence
//
//  Created by dae2 on 2019-10-21.
//  Copyright © 2019 dae2. All rights reserved.
//

//base64EncodedString() pour insert car passwd est 0 live

import UIKit

class InscriptionViewController: UIViewController
{
    var valide = false
    @IBOutlet weak var SpecialityView: UIView!
    @IBOutlet weak var Doc_InfSegmentControl: UISegmentedControl!
    var NurseTab: [Nurse] = []
    var DocTab: [Doctor] = []
    @IBOutlet weak var Last_NameTextField: UITextField!
    @IBOutlet weak var First_NameTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var Confirmation_PasswordTextField: UITextField!
    @IBOutlet weak var SpecialityTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectDocOrNurse(_ sender: Any)
    {
        if(Doc_InfSegmentControl.selectedSegmentIndex == 0)
        {
            SpecialityView.isHidden = false;
        }
        else
        {
            SpecialityView.isHidden = true;
        }
    }
    
    func showId()
    {
        var Message_Id = "";
        if(Doc_InfSegmentControl.selectedSegmentIndex == 0)
        {
            MarthaRequest.fetchDoctorId(id: 0, completion:
            {
                (doctors) in
                if let fetchDoctors = doctors
                {
                    self.DocTab = fetchDoctors
                    DispatchQueue.main.async
                    {
                        Message_Id = (String)(self.DocTab[0].Doctor_id)
                        let Alert = UIAlertController(title: "Votre Numéro d'employé est :", message: Message_Id, preferredStyle: .alert)
                                
                        Alert.addAction(UIAlertAction(title: "OK", style: .default , handler:
                        {(_) in
                            self.performSegue(withIdentifier: "Show_main", sender: nil);
                                        
                        }))
                        self.present(Alert, animated: true)
                    }
                }
            })
        }
        else
        {
            MarthaRequest.fetchNursesId(id: 0, completion:
                {
                    (nurses) in
                    if let fetchNurses = nurses
                    {
                        self.NurseTab = fetchNurses
                        DispatchQueue.main.async
                            {
                                Message_Id = (String)(self.NurseTab[0].Nurse_Id)
                                let Alert = UIAlertController(title: "Id", message: Message_Id, preferredStyle: .alert)
                                
                                Alert.addAction(UIAlertAction(title: "OK", style: .default , handler:
                                    {(_) in
                                        self.performSegue(withIdentifier: "Show_main", sender: nil);
                                        
                                }))
                                self.present(Alert, animated: true)
                        }
                    }
            })
        }
    }
    
    @IBAction func inscriptionEmploye(_ sender: Any)
    {
        var Message_Erreur = "";
        if(Last_NameTextField.text == "")
        {
            Message_Erreur = "Nom vide";
        }
        else if(First_NameTextField.text == "")
        {
            Message_Erreur = "Prenom vide";
        }
        else if(PasswordTextField.text == "")
        {
            Message_Erreur = "Mot de passe vide";
        }
        else if(Confirmation_PasswordTextField.text == "")
        {
            Message_Erreur = "Confirmation Mot de passe vide";
        }
        else if(PasswordTextField.text != Confirmation_PasswordTextField.text)
        {
            Message_Erreur = "Mot de passe non identique";
        }
        else
        {
            valide = true;
        }
        
        if(valide)
        {
            if(Doc_InfSegmentControl.selectedSegmentIndex == 0)
            {
                MarthaRequest.insertDoc(last_name: self.Last_NameTextField.text!, first_name: self.First_NameTextField.text!, passwd: self.Confirmation_PasswordTextField.text ?? " ", speciality: self.SpecialityTextField.text ?? " ")
                { _ in
                    
                    DispatchQueue.main.async
                    {
                        self.showId();
                    }
                }
            }
            else
            {
                MarthaRequest.insertNurse(last_name: self.Last_NameTextField.text!, first_name: self.First_NameTextField.text!, passwd: self.Confirmation_PasswordTextField.text!)
                { _ in
                    
                    DispatchQueue.main.async
                    {
                        self.showId();
                    }
                }
            }
        }
        else
        {
            let Alert = UIAlertController(title: "Erreur", message: Message_Erreur, preferredStyle: .alert)
            
            Alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
            {
                (_) in
                self.PasswordTextField.text = ""
                self.Confirmation_PasswordTextField.text = ""
            }))
            self.present(Alert, animated: true)
        }
    }
}
