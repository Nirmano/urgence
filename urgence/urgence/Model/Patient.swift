//
//  Patient.swift
//  urgence
//
//  Created by dae2 on 2019-11-22.
//  Copyright © 2019 dae2. All rights reserved.
//

import Foundation

struct Patient {
    
    var NAM: String
    var Last_Name: String
    var First_Name: String
    var Gender: String
    var Birthday: String
    
    init?(json:[String:Any])
    {
        
        if let NAM = json["NAM"] as? String,
            let Last_Name = json["Last_name"] as? String,
            let First_Name = json["First_name"] as? String,
            let Gender = json["Gender"] as? String,
            let Birthday = json["Birthday"] as? String{
            
            self.NAM = NAM
            self.First_Name = First_Name
            self.Last_Name = Last_Name
            self.Birthday = Birthday
            self.Gender = Gender
        }
        else
        {
            return nil
        }
    }
}
