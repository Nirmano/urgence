//
//  Infirmiere.swift
//  urgence
//
//  Created by dae2 on 2019-11-08.
//  Copyright © 2019 dae2. All rights reserved.
//

import Foundation

struct Nurse {
    
    var Nurse_Id: Int
    var Password: String
    var Last_Name: String
    var First_Name: String
    
    init?(json:[String:Any])
    {
        
        if let Nurse_Id = json["Nurse_id"] as? Int,
            let Password = json["Passwd"] as? String,
                let Last_Name = json["Last_name"] as? String,
                    let First_Name = json["First_name"] as? String{
            
            self.Nurse_Id = Nurse_Id
            self.First_Name = First_Name
            self.Password = Password
            self.Last_Name = Last_Name
        }
        else
        {
            return nil
        }
    }
}
