//
//  MarthaRequest.swift
//  urgence
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 dae2. All rights reserved.
//

import Foundation

class MarthaRequest{
    
    private static let auth = Data("team1:VYhhMocu4y".utf8).base64EncodedString()
    
    private static func request(query: String,params: Data? = nil , completion:@escaping ([String:Any]?)->Void)
    {
        let url = URL(string: "http://martha.jh.shawinigan.info/queries/\(query)/execute")!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        request.httpBody = params
        request.httpMethod  = "POST"
        request.addValue(MarthaRequest.auth, forHTTPHeaderField: "auth")
        
        let task = session.dataTask(with: request as URLRequest) { (httpData, httpResponse, httpError) in
            
            if let error = httpError{
                print(error)
                completion(nil)
                
            }else if let data = httpData{
                do{
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? [String:Any]{
                        completion(json)
                    }
                    else{
                        
                        print("invalid json")
                        completion(nil)
                    }
                    
                }catch let parsingError{
                    print("Parsing error: \(parsingError)")
                    completion(nil)
                }
                
            }else {
                
                print("Unknow error")
                completion(nil)
            }
        }
        
        task.resume()
        
    }
    
    
    
    public static func  fetchDoctor(id: Int,completion: @escaping ([Doctor]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["id": id])
        request(query: "select-doctorAuth", params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let doctorJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var doctors: [Doctor] = []
                    
                    for doctorJson in doctorJson {
                        
                        if let doctorJsonObject = doctorJson as? [String:Any],
                            let doctor = Doctor(json: doctorJsonObject){
                            
                            doctors.append(doctor)
                            
                        }
                    }
                    completion(doctors)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  fetchRoom(id: Int,completion: @escaping ([Room]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["id": id])
        request(query: "select-RoomDoc",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  fetchNurse(id: Int,passwd: String,completion: @escaping ([Nurse]?)->Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["passwd": passwd,"id": id])
        request(query: "Nurse_unkown", params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let infirmieresJson = jsonObject["data"] as? [Any]
            {
                if(succes)
                {
                    var infirmieres: [Nurse] = []
                    
                    for infirmiereJson in infirmieresJson
                    {
                        if let infirmiereJson = infirmiereJson as? [String:Any],
                            let infirmiere = Nurse(json: infirmiereJson)
                        {
                            infirmieres.append(infirmiere)
                        }
                    }
                    completion(infirmieres)
                }
                else
                {
                    print("Fetch failed")
                    completion(nil)
                }
                print(jsonObject)
            }
            else
            {
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  fetchDoctorLog(id: Int,passwd: String,completion: @escaping ([Doctor]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "passwd": passwd,"id": id])
        
        request(query: "Doclog",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let doctorJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var doctors: [Doctor] = []
                    
                    for doctorJson in doctorJson {
                        
                        if let doctorJsonObject = doctorJson as? [String:Any],
                            let doctor = Doctor(json: doctorJsonObject){
                            
                            doctors.append(doctor)
                            
                        }
                    }
                    completion(doctors)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  insertDoc(last_name: String,first_name: String,passwd: String,speciality: String,completion: @escaping ([Doctor]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "passwd": passwd,"first_name": first_name,"last_name": last_name,"speciality":speciality])
        
        request(query: "insert-Doc",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let doctorJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var doctors: [Doctor] = []
                    
                    for doctorJson in doctorJson {
                        
                        if let doctorJsonObject = doctorJson as? [String:Any],
                            let doctor = Doctor(json: doctorJsonObject){
                            
                            doctors.append(doctor)
                            
                        }
                    }
                    completion(doctors)
                }else{
                    
                    print("insert failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  insertNurse(last_name: String,first_name: String,passwd: String,completion: @escaping ([Nurse]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "passwd": passwd,"first_name": first_name,"last_name": last_name])
        
        request(query: "insert-Nurse",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let nurseJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var nurses: [Nurse] = []
                    
                    for nurseJson in nurseJson {
                        
                        if let nurseJsonObject = nurseJson as? [String:Any],
                            let nurse = Nurse(json: nurseJsonObject){
                            
                            nurses.append(nurse)
                            
                        }
                    }
                    completion(nurses)
                }else{
                    
                    print("insert failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    
    public static func  updateDoc(id: Int ,last_name: String,first_name: String,passwd: String,speciality: String,completion: @escaping ([Doctor]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "passwd": passwd,"first_name": first_name,"last_name": last_name,"speciality":speciality,"id":id])
        
        request(query: "update-Doc",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let doctorJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var doctors: [Doctor] = []
                    
                    for doctorJson in doctorJson {
                        
                        if let doctorJsonObject = doctorJson as? [String:Any],
                            let doctor = Doctor(json: doctorJsonObject){
                            
                            doctors.append(doctor)
                            
                        }
                    }
                    completion(doctors)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  updateNurse(id: Int ,last_name: String,first_name: String,passwd: String,completion: @escaping ([Nurse]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "passwd": passwd,"first_name": first_name,"last_name": last_name,"id":id])
        
        request(query: "update-Nurse",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let nurseJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var nurses: [Nurse] = []
                    
                    for nurseJson in nurseJson {
                        
                        if let nurseJsonObject = nurseJson as? [String:Any],
                            let nurse = Nurse(json: nurseJsonObject){
                            
                            nurses.append(nurse)
                            
                        }
                    }
                    completion(nurses)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  deleteDoc(id: Int ,completion: @escaping ([Doctor]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "id": id])
        
        request(query: "delete-Doc",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let doctorJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var doctors: [Doctor] = []
                    
                    for doctorJson in doctorJson {
                        
                        if let doctorJsonObject = doctorJson as? [String:Any],
                            let doctor = Doctor(json: doctorJsonObject){
                            
                            doctors.append(doctor)
                            
                        }
                    }
                    completion(doctors)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    
    
    
    public static func  deleteNurse(id: Int ,completion: @escaping ([Nurse]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: ["id":id])
        
        request(query: "delete-Nurse",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let nurseJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var nurses: [Nurse] = []
                    
                    for nurseJson in nurseJson {
                        
                        if let nurseJsonObject = nurseJson as? [String:Any],
                            let nurse = Nurse(json: nurseJsonObject){
                            
                            nurses.append(nurse)
                            
                        }
                    }
                    completion(nurses)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  deleteRoom(id: Int ,completion: @escaping ([Room]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "id": id])
        
        request(query: "delete-Room",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  deleteCase(nam: String ,completion: @escaping ([Cases]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "nam": nam])
        
        request(query: "delete-Cases",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let casesJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var cases: [Cases] = []
                    
                    for casesJson in casesJson {
                        
                        if let caseJsonObject = casesJson as? [String:Any],
                            let casee = Cases(json: caseJsonObject){
                            
                            cases.append(casee)
                            
                        }
                    }
                    completion(cases)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  deletePatient(nam: String ,completion: @escaping ([Patient]?)->Void){
        
        let params = try! JSONSerialization.data(withJSONObject: [ "nam": nam])
        
        request(query: "delete-Patient",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let patientJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var patients: [Patient] = []
                    
                    for patientJson in patientJson {
                        
                        if let patientJsonObject = patientJson as? [String:Any],
                            let patient = Patient(json: patientJsonObject){
                            
                            patients.append(patient)
                            
                        }
                    }
                    completion(patients)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  insertRoom(doctor_id: Int,completion: @escaping ([Room]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["doctor_id": doctor_id])
        request(query: "insert-Room",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("insert failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  updateRoom(room_id: Int,case_id: Int,completion: @escaping ([Room]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["room_id": room_id,"case_id":case_id])
        request(query: "update-Room",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  updateRoomPatNul(NAM: String,completion: @escaping ([Room]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["NAM": NAM])
        request(query: "update-RoomPatNull",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  updateRoomPutDoc(room_id: Int,doctor_id: Int,completion: @escaping ([Room]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["Room_id": room_id,"Doctor_id":doctor_id])
        request(query: "update-RoomPutDoc",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  updateRoomNoDoc(Doctor_id: Int,completion: @escaping ([Room]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["Doctor_id":Doctor_id])
        request(query: "update-RoomNoDoc",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  fetchDoctorId(id: Int,completion: @escaping ([Doctor]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["id": id])
        request(query: "select-DoctorId", params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let doctorJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var doctors: [Doctor] = []
                    
                    for doctorJson in doctorJson {
                        
                        if let doctorJsonObject = doctorJson as? [String:Any],
                            let doctor = Doctor(json: doctorJsonObject){
                            
                            doctors.append(doctor)
                            
                        }
                    }
                    completion(doctors)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  fetchNursesId(id: Int,completion: @escaping ([Nurse]?)->Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["id": id])
        request(query: "select-NurseId", params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let infirmieresJson = jsonObject["data"] as? [Any]
            {
                if(succes)
                {
                    var infirmieres: [Nurse] = []
                    
                    for infirmiereJson in infirmieresJson
                    {
                        if let infirmiereJson = infirmiereJson as? [String:Any],
                            let infirmiere = Nurse(json: infirmiereJson)
                        {
                            infirmieres.append(infirmiere)
                        }
                    }
                    completion(infirmieres)
                }
                else
                {
                    print("Fetch failed")
                    completion(nil)
                }
                print(jsonObject)
            }
            else
            {
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  fetchRoomAll(id: Int,completion: @escaping ([Room]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["id": id])
        request(query: "select-RoomAll",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let roomJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var rooms: [Room] = []
                    
                    for roomJson in roomJson {
                        
                        if let roomJsonObject = roomJson as? [String:Any],
                            let room = Room(json: roomJsonObject){
                            
                            rooms.append(room)
                            
                        }
                    }
                    completion(rooms)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  fetchCases(id: Int,completion: @escaping ([Cases]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["id": id])
        request(query: "select-Patient",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let caseJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var cases: [Cases] = []
                    
                    for caseJson in caseJson {
                        
                        if let caseJsonObject = caseJson as? [String:Any],
                            let casee = Cases(json: caseJsonObject){
                            
                            cases.append(casee)
                            
                        }
                    }
                    completion(cases)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    

    public static func  insertPatient(nam: String,last_name: String,first_name: String, gender: String,birthday: String ,completion: @escaping ([Patient]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["nam": nam,"last_name": last_name,"first_name": first_name,"date":birthday, "gender": gender])
        request(query: "insert-Patient",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let patientJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var patients: [Patient] = []
                    
                    for patientJson in patientJson {
                        
                        if let patientJsonObject = patientJson as? [String:Any],
                            let patient = Patient(json: patientJsonObject){
                            
                            patients.append(patient)
                            
                        }
                    }
                    completion(patients)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    public static func  insertCases(nam: String,nurse_id: Int, priority: Int,completion: @escaping ([Cases]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["nam": nam, "priority":priority , "nurse_id":nurse_id])
        request(query: "insert-Case",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let caseJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var cases: [Cases] = []
                    
                    for caseJson in caseJson {
                        
                        if let caseJsonObject = caseJson as? [String:Any],
                            let casee = Cases(json: caseJsonObject){
                            
                            cases.append(casee)
                            
                        }
                    }
                    completion(cases)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  fetchPatient(id: String,completion: @escaping ([Patient]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["nam": id])
        request(query: "select-onePatient",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let patientJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var patients: [Patient] = []
                    
                    for patientJson in patientJson {
                        
                        if let patientJsonObject = patientJson as? [String:Any],
                            let patiente = Patient(json: patientJsonObject){
                            
                            patients.append(patiente)
                            
                        }
                    }
                    completion(patients)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    public static func  fetchPatientwithCases(id: Int,completion: @escaping ([Cases]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["id": id])
        request(query: "select-PatientWCases",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let patientJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var patients: [Cases] = []
                    
                    for patientJson in patientJson {
                        
                        if let patientJsonObject = patientJson as? [String:Any],
                            let patiente = Cases(json: patientJsonObject){
                            
                            patients.append(patiente)
                            
                        }
                    }
                    completion(patients)
                }else{
                    
                    print("Fetch failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
    
    
    public static func  updateCasesPat(NAM: String,priority: Int,completion: @escaping ([Cases]?)->Void){
        let params = try! JSONSerialization.data(withJSONObject: ["NAM":NAM, "priority":priority])
        request(query: "update-Patient",params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,let succes = jsonObject["success"] as? Bool, let caseJson = jsonObject["data"] as? [Any] {
                
                if(succes){
                    var cases: [Cases] = []
                    
                    for caseJson in caseJson {
                        
                        if let caseJsonObject = caseJson as? [String:Any],
                            let casee = Cases(json: caseJsonObject){
                            
                            cases.append(casee)
                            
                        }
                    }
                    completion(cases)
                }else{
                    
                    print("update failed")
                    completion(nil)
                }
                
                
                print(jsonObject)
                
            }else{
                print("Request failed, invalid format")
                completion(nil)
            }
        }
    }
    
    
}
