//
//  Patient.swift
//  urgence
//
//  Created by dae2 on 2019-11-22.
//  Copyright © 2019 dae2. All rights reserved.
//

//faire en sorte que cases_id peut accepter des null

import Foundation

struct Cases {
    
    var Cases_Id: Int?
    var NAM: String
    var Priority: Int
    var Nurse_Id: Int
   
    
    init?(json:[String:Any])
    {
        
        if  let NAM = json["NAM"] as? String,
            let Nurse_Id = json["Nurse_id"] as? Int,
            let Priority = json["Priority"] as? Int{
            
            self.NAM = NAM
            self.Cases_Id = json["Cases_id"] as? Int
            self.Nurse_Id = Nurse_Id
            self.Priority = Priority
        }
        else
        {
            return nil
        }
  
    }
}
