//
//  Doctor.swift
//  urgence
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 dae2. All rights reserved.
//

import Foundation

struct Doctor {
    
    var Doctor_id: Int
    var Password: String
    var Last_Name: String
    var First_Name: String
    var Speciality: String
    
    init?(json:[String:Any]) {
        
            if let Doctor_Id = json["Doctor_id"] as? Int,
                    let Password = json["Passwd"] as? String,
                     let Last_Name = json["Last_name"] as? String,
                            let First_Name = json["First_name"] as? String,
                                let Speciality = json["Speciality"] as? String{
                
                self.Doctor_id = Doctor_Id
                self.First_Name = First_Name
                self.Password = Password
                self.Last_Name = Last_Name
                self.Speciality = Speciality
                
            }else{
                return nil
                
        }
        
    }
    
}
