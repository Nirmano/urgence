


INSERT INTO DOCTOR(Last_name,First_name,Passwd,Speciality) VALUES
('Pépin','Alexandre','1','Urgentologue');
INSERT INTO DOCTOR(Last_name,First_name,Passwd,Speciality) VALUES
('abc','abc','1','Cardiologue');
INSERT INTO DOCTOR(Last_name,First_name,Passwd,Speciality) VALUES
('Reucherand','Antho','1','Jemencrissologue');
INSERT INTO DOCTOR(Last_name,First_name,Passwd,Speciality) VALUES
('abc','abc','1','Neurologue');
INSERT INTO DOCTOR(Last_name,First_name,Passwd,Speciality) VALUES
('abc','abc','1','Endocrinologue');





INSERT INTO PATIENT VALUES
('PEPA96062110','Pepino','Alex','Octopus','1996-06-21');
INSERT INTO PATIENT VALUES
('LAMC56081209','Lamouche','Carl','Homme','1956-08-12');
INSERT INTO PATIENT VALUES
('CLAM00010107','Clark','Mathieu','Chien','2000-01-01');
INSERT INTO PATIENT VALUES
('FRAB94032103','Francoeur','Bob','Trans','1994-03-21');
INSERT INTO PATIENT VALUES
('POIC76062006','Poirier','Claude','Femme','1976-06-20');


INSERT INTO NURSE(Last_name,First_name,Passwd) VALUES
('Junior','Carole','a');
INSERT INTO NURSE(Last_name,First_name,Passwd) VALUES
('Bob','Jean','a');
INSERT INTO NURSE(Last_name,First_name,Passwd) VALUES
('Bob','Bob','a');
INSERT INTO NURSE(Last_name,First_name,Passwd) VALUES
('Admin','Admin','a');
INSERT INTO NURSE(Last_name,First_name,Passwd) VALUES
('APP','APP','a');
INSERT INTO NURSE(Last_name,First_name,Passwd) VALUES
('ASD','ASD','a');






INSERT INTO CASES(NAM,Priority,Nurse_id) VALUES
('PEPA96062110',1,1);
INSERT INTO CASES(NAM,Priority,Nurse_id) VALUES
('LAMC56081209',2,2);
INSERT INTO CASES(NAM,Priority,Nurse_id) VALUES
('CLAM00010107',3,2);
INSERT INTO CASES(NAM,Priority,Nurse_id) VALUES
('FRAB94032103',4,3);
INSERT INTO CASES(NAM,Priority,Nurse_id) VALUES
('POIC76062006',1,4);



INSERT INTO ROOM(Cases_id,Doctor_id) VALUES
(1,1);
INSERT INTO ROOM (Cases_id,Doctor_id) VALUES
(2,2);
INSERT INTO ROOM (Cases_id,Doctor_id) VALUES
(3,3);
INSERT INTO ROOM (Cases_id,Doctor_id) VALUES
(4,4);
INSERT INTO ROOM (Cases_id,Doctor_id) VALUES
(NULL,5);
