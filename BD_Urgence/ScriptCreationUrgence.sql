
CREATE TABLE DOCTOR
(
	Doctor_id		int 							NOT NULL AUTO_INCREMENT,
	Last_name		varchar(15)				NOT NULL,
	First_name	varchar(30)				NOT NULL,
	Passwd			varchar(20)				NOT NULL,
	Speciality 	varchar(20)				NOT NULL,
	PRIMARY KEY (Doctor_id)

)ENGINE=INNODB;



CREATE TABLE NURSE
(
  Nurse_id      int 							NOT NULL AUTO_INCREMENT,
	Last_name			varchar(50)				NOT NULL,
	First_name    varchar(15) 			NOT NULL,
	Passwd				varchar(20)				NOT NULL,
	PRIMARY KEY (Nurse_id)
)ENGINE=INNODB;



CREATE TABLE PATIENT
(
  NAM						varchar(12)		NOT NULL,
	Last_name			varchar(15)		NOT NULL,
	First_name		varchar(30)		NOT NULL,
	Gender				varchar(10)		NOT NULL,
	Birthday			Date				 	NOT NULL,
	PRIMARY KEY (NAM)

)ENGINE=INNODB;


CREATE TABLE CASES
(
	Cases_id    int 			NOT NULL AUTO_INCREMENT,
	NAM					varchar(12) 			NOT NULL,
	Priority		tinyint		NOT NULL,
	Nurse_id		int 			NOT NULL,
	PRIMARY KEY (Cases_id),
	FOREIGN KEY (NAM) REFERENCES PATIENT(NAM),
	FOREIGN KEY (Nurse_id) REFERENCES NURSE(Nurse_id)

)ENGINE=INNODB;





CREATE TABLE ROOM
(
	Room_id		int NOT NULL AUTO_INCREMENT,
	Cases_id		int,
	Doctor_id int ,
	PRIMARY KEY (Room_id),
	FOREIGN KEY (Cases_id) REFERENCES CASES(Cases_id),
	FOREIGN KEY (Doctor_id) REFERENCES DOCTOR(Doctor_id)
)ENGINE=INNODB;
